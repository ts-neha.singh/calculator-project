package com.rakuten.example;

public class StandardCalculator implements BasicCalculator {
    @Override
    public Double add(Double input1, Double input2) {
        return (input1 + input2);
    }

    @Override
    public Double substract(Double input1, Double input2) {
        return (input1 - input2);
    }

    @Override
    public Double multiply(Double input1, Double input2) {
       return (input1 * input2);
    }

    @Override
    public Double division(Double input1, Double input2) {
        return (input1 / input2);

    }
}

