package com.rakuten.example;

public class ScientificCalculator extends  StandardCalculator {


    private static String[] stdOperator = {"sqrt", "cbrt", "fact", "cube", "sqre", "sin", "cos", "tan", "log"};

    public static boolean isValidOperator(String operator) {
        for (String opt : stdOperator) {
            if (operator.equals(opt)) {
                return true;
            }
        }
        return false;
    }

    public  static double calculate(double operand1, String operator) {
        double result = 0.0;

        switch (operator) {
            case "sqrt":
                if (operand1 >= 0) {
                    result = Math.sqrt(operand1);
                }
                break;
            case "sqre":
                result = (operand1* operand1);
                break;
            case "sine":
                result = Math.sin((operand1 * Math.PI));
                break;

            case "tan":
                result = Math.tan((operand1 * Math.PI) / 180);
                break;
            case "cos":
                result = Math.cos((operand1 * Math.PI) / 180);
                break;


            default:

        }
        return result;


    }


}



