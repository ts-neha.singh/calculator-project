package com.rakuten.example;

public interface BasicCalculator {

        public Double add(Double input1, Double input2);

        public Double substract(Double input1, Double input2);

        public Double multiply(Double input1, Double input2);

        public Double division(Double input1, Double input2);


}
