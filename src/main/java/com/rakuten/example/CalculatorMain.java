package com.rakuten.example;

import java.util.*;


public  class CalculatorMain {



        private static BasicCalculator standardCalculator = new StandardCalculator();

        public static final String OPR_ADD = "+";

        public static final String OPR_SUBSTRACT = "-";

        public static final String OPR_MULTIPLY = "*";

        public static final String OPR_DIVIDE = "/";

        public static void main (String [] args) {

            Scanner sc = new Scanner(System.in);
            Double result = 0.0;
            System.out.println("Enter input");
            String input = sc.nextLine();
            if(input != null && !input.trim().isEmpty()) {
                Queue queue = new LinkedList();
                // convert input into char array
                char [] inputChars = input.toCharArray();

                // add in queue
                for(char ch : inputChars) {
                    queue.add(Character.toString(ch));
                }
                // queue iterator
                Iterator<String> it = queue.iterator();

                // get first operand and store in result
                String operand1 = "";
                for (int i = 0; i < queue.size(); i++){
                    if(getStandardOperations().contains(queue.peek())){
                        break;
                    } else {
                        operand1 = operand1+queue.remove();
                    }
                }
                if (operand1 != null && !operand1.trim().isEmpty()){
                    try {
                        Double op1 = Double.parseDouble(operand1);
                        result = (result+op1);
                    } catch (Exception ex) {
                        System.out.println("Please enter valid input1");
                    }
                }

                // get operation type and second operand and perform operation with switch case
                while (it.hasNext()){
                    if(getStandardOperations().contains(queue.peek())){
                        String operand2 = "";
                        String operation = (String) queue.remove();
                        while (it.hasNext()){
                            if(getStandardOperations().contains(queue.peek())){
                                break;
                            } else {
                                operand2 = operand2+queue.remove();
                            }
                        }
                        Double op2 = null;
                        if (operand2 != null && !operand2.trim().isEmpty()){
                            try {
                                op2 = Double.parseDouble(operand2);
                            } catch (Exception ex) {
                                System.out.println("Please enter valid input2");
                            }
                        }
                        switch (operation) {
                            case OPR_ADD:
                                result = standardCalculator.add(result, op2);
                                break;
                            case OPR_SUBSTRACT:
                                result = standardCalculator.substract(result, op2);
                                break;
                            case OPR_MULTIPLY:
                                result = standardCalculator.multiply(result, op2);
                                break;
                            case OPR_DIVIDE:
                                result = standardCalculator.division(result, op2);
                                break;
                            default:
                                System.out.println("Wrong operation type");
                        }
                    }
                }
                System.out.println("result is : " + result);
            }  else {
                System.out.println("Enter Valid input");
            }
        }

        public static List<String> getStandardOperations(){
            List<String> oprs = new ArrayList<>();
            oprs.add(OPR_ADD);
            oprs.add(OPR_SUBSTRACT);
            oprs.add(OPR_DIVIDE);
            oprs.add(OPR_MULTIPLY);
            return oprs;
        }

    }


